import Header from "./component/Header";
import SearchComponent from "./component/SearchComponent";
import FilterComponent from "./component/FilterComponent";
import CountryCards from "./component/CountryCards";
import React from "react";
import { Route, Routes } from "react-router-dom";
import "./App.css";
import CountryDetail from "./component/CountryDetail";

export default function App() {
  const [countriesData, setCountriesData] = React.useState([]);
  const [searchCountry, setSearchCountry] = React.useState("");
  const [filterRegion, setFilterRegion] = React.useState("");
  const [darkMode, setDarkMode] = React.useState(false);

  React.useEffect(() => {
    async function fetchData() {
      try {
        const response = await fetch("https://restcountries.com/v3.1/all");
        const data = await response.json();
        setCountriesData(data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    }
    fetchData();
  }, []);

  const filterCountries = countriesData
    .filter((country) =>
      filterRegion === "" ? true : country.region === filterRegion
    )
    .filter((country) =>
      country.name.common.toLowerCase().includes(searchCountry.toLowerCase())
    );

  const darkModeStyle = {
    backgroundColor: darkMode ? "#212E37" : "#fff",
    color: darkMode ? "#fff" : "#000",
  };

  document.body.style.backgroundColor = darkMode ? "#212E37" : "#FAFAFA";

  return (
    <div>
      <Header setDarkMode={setDarkMode} darkMode={darkMode} />
      {console.log(darkMode)}
      <main>
        <Routes>
          <Route
            path="/"
            element={
              <div>
                <div className="search-and-filter" style={darkModeStyle}>
                  <SearchComponent
                    darkMode={darkMode}
                    searchCountry={searchCountry}
                    setSearchCountry={setSearchCountry}
                  />
                  <FilterComponent
                    darkMode={darkMode}
                    filterRegion={filterRegion}
                    setFilterRegion={setFilterRegion}
                  />
                </div>
                <div className="countries">
                  <CountryCards
                    countriesData={filterCountries}
                    darkMode={darkMode}
                  />
                </div>
              </div>
            }
          ></Route>
          <Route
            path="/country/:cca3"
            element={<CountryDetail darkMode={darkMode} />}
          ></Route>
        </Routes>
      </main>
    </div>
  );
}
