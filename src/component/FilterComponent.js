export default function FilterComponent({
  filterRegion,
  setFilterRegion,
  darkMode,
}) {
  const regions = [
    "Africa",
    "Americas",
    "Antarctic",
    "Asia",
    "Europe",
    "Oceania",
  ];

  const darkModeButton = {
    backgroundColor: darkMode ? "#2B3743" : "#fff",
    color: darkMode ? "#fff" : "#000",
    boxShadow: darkMode ? "none" : "5px 4px 8px grey",
    border: "none",
  };
  return (
    <select
      id="filter-by-region"
      style={darkModeButton}
      value={filterRegion}
      onChange={(e) => {
        setFilterRegion(e.target.value);
      }}
    >
      <option value="">All Regions</option>
      {regions.map((region) => (
        <option key={region} value={region}>
          {region}
        </option>
      ))}
    </select>
  );
}
