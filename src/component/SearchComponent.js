export default function SearchComponent({
  searchCountry,
  setSearchCountry,
  darkMode,
}) {
  const darkModeButton = {
    backgroundColor: darkMode ? "#2B3743" : "#fff",
    color: darkMode ? "#fff" : "#000",
    boxShadow: darkMode ? "none" : "5px 4px 8px grey",
  };
  return (
    <div className="search-field" style={darkModeButton}>
      <span>
        <img src="./images/search_icon.svg" alt="search_icon" />
      </span>
      <input
        style={{
          ...darkModeButton,
          boxShadow: "none",
        }}
        type="text"
        placeholder="Search for a country"
        value={searchCountry}
        onChange={(e) => {
          setSearchCountry(e.target.value);
        }}
      />
    </div>
  );
}
