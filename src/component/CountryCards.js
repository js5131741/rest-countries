import { Link } from "react-router-dom";

export default function CountryCards({ countriesData, darkMode }) {
  const sortedCountries = countriesData
    ? [...countriesData].sort((a, b) => {
        const nameA = a.name.common.toLowerCase();
        const nameB = b.name.common.toLowerCase();
        return nameA.localeCompare(nameB);
      })
    : [];

  const darkModeToggle = {
    backgroundColor: darkMode ? "#212E37" : "#fafafa",
    color: darkMode ? "#fff" : "#000",
  };

  const darkModeCards = {
    backgroundColor: darkMode ? "#2B3743" : "#fff",
    color: darkMode ? "#fff" : "#000",
  };

  return (
    <div className="countries-container" style={darkModeToggle}>
      {sortedCountries.length !== 0 ? (
        sortedCountries.map((country) => (
          <Link
            to={`/country/${country.cca3}`}
            className="links"
            key={country.name.common}
          >
            <div
              className="country-card"
              key={country.name.common}
              style={darkModeCards}
            >
              <div className="flag">
                <img src={country.flags.png} alt="flag here" />
              </div>
              <div className="country-name">
                <h3>{country.name.common}</h3>
              </div>
              <div className="country-info">
                <div className="card-info">
                  <h4 className="country-heading">Population:&nbsp;</h4>
                  <p className="info">{country.population}</p>
                </div>
                <div className="card-info">
                  <h4 className="country-heading">Region:&nbsp;</h4>
                  <p className="info">{country.region}</p>
                </div>
                <div className="card-info last-child">
                  <h4 className="country-heading">Capital:&nbsp;</h4>
                  <p className="info">{country.capital}</p>
                </div>
              </div>
            </div>
          </Link>
        ))
      ) : (
        <div className="no-data">No Countries Found</div>
      )}
    </div>
  );
}
