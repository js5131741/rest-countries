import { Link, useParams } from "react-router-dom";
import React from "react";

export default function CountryDetail({ darkMode }) {
  const [countriesData, setCountriesData] = React.useState([]);
  React.useEffect(() => {
    async function fetchData() {
      try {
        const response = await fetch("https://restcountries.com/v3.1/all");
        const data = await response.json();
        setCountriesData(data);
      } catch (error) {
        console.error("Error fetching data:", error);
      }
    }
    fetchData();
  }, []);

  const { cca3 } = useParams();
  const country = countriesData.find((item) => item.cca3 === cca3);
  const darkModeToggle = {
    backgroundColor: darkMode ? "#212E37" : "#fafafa",
    color: darkMode ? "#fff" : "#000",
  };

  const darkModeButton = {
    backgroundColor: darkMode ? "#2B3743" : "#fff",
    color: darkMode ? "#fff" : "#000",
    boxShadow: darkMode ? "none" : "5px 4px 8px grey",
  };
  return countriesData.length === 0 ? (
    "None"
  ) : (
    <div className="selected-country" style={darkModeToggle}>
      <Link to={`/`}>
        <button className="back-button" style={darkModeButton}>
          Go Back
        </button>
      </Link>
      <div className="country-detail">
        <img src={country.flags.svg} alt="flag here" />

        <div className="country-detail-text">
          <h3>{country.name.common}</h3>
          <div className="column">
            <div className="country-detail-column1">
              <div className="country-detail-info">
                <span className="bold">Native Name: </span>
                <span>
                  {Object.keys(country.name).includes("nativeName")
                    ? Object.values(country.name.nativeName)[0].official
                    : "None"}
                </span>
              </div>
              <div className="country-detail-info">
                <span className="bold">Population: </span>
                <span>{country.population}</span>
              </div>
              <div className="country-detail-info">
                <span className="bold">Region: </span>
                <span>{country.region}</span>
              </div>
              <div className="country-detail-info">
                <span className="bold">Sub Region: </span>
                <span>
                  {Object.keys(country).includes("subregion")
                    ? Object.values(country.subregion)
                    : "None"}
                </span>
              </div>
              <div className="country-detail-info">
                <span className="bold">Capital: </span>
                <span>
                  {Object.keys(country).includes("capital")
                    ? Object.values(country.capital[0])
                    : "None"}
                </span>
              </div>
            </div>
            <div className="country-detail-column2">
              <div className="country-detail-info">
                <span className="bold">Top Level Domain: </span>
                <span>{country.tld[0]}</span>
              </div>
              <div className="country-detail-info">
                <span className="bold">Currencies: </span>
                <span>
                  {Object.keys(country).includes("currencies")
                    ? Object.values(country.currencies)[0].name
                    : ""}
                </span>
              </div>
              <div className="country-detail-info">
                <span className="bold">Languages: </span>
                <span>
                  {Object.keys(country).includes("languages")
                    ? Object.values(country.languages).toString()
                    : ""}
                </span>
              </div>
            </div>
          </div>
          <div className="border-countries">
            <span className="bold">Border Countries: </span>
            {Object.keys(country).includes("borders")
              ? country.borders.map((item) => (
                  <Link key={item} to={`/country/${item}`}>
                    <button className="border-buttons" style={darkModeButton}>
                      {
                        countriesData.find((country) => country.cca3 === item)
                          .name.common
                      }
                    </button>
                  </Link>
                ))
              : "None"}
          </div>
        </div>
      </div>
    </div>
  );
}
