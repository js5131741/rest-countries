export default function Header({ darkMode, setDarkMode }) {
  const headerStyle = {
    backgroundColor: darkMode ? "#2B3743" : "#fff",
    color: darkMode ? "#fff" : "#000",
  };
  return (
    <header style={headerStyle}>
      <h1>Where in the world?</h1>
      {darkMode ? (
        <div className="screen-mode" onClick={() => setDarkMode(!darkMode)}>
          <img src="../images/light_mode.svg" alt="dark_mode_svg" />
          <span>Light Mode</span>
        </div>
      ) : (
        <div className="screen-mode" onClick={() => setDarkMode(!darkMode)}>
          <img src="../images/dark_mode.svg" alt="dark_mode_svg" />
          <span>Dark Mode</span>
        </div>
      )}
    </header>
  );
}
