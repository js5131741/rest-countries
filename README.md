## Getting started

### Steps

1. Clone the repository.

```bash
git clone https://gitlab.com/js5131741/rest-countries.git
```

2. cd into the directory and install all dependencies.

```bash
npm install
```

3. Run the app with the following command

```bash
npm start
```
